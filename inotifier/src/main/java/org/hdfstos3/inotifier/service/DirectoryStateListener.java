package org.hdfstos3.inotifier.service;

import lombok.SneakyThrows;
import org.apache.hadoop.hdfs.DFSInotifyEventInputStream;
import org.apache.hadoop.hdfs.inotify.Event;
import org.apache.hadoop.hdfs.inotify.EventBatch;
import org.hdfstos3.models.kafka.KafkaMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
public class DirectoryStateListener {

    @Autowired
    private DFSInotifyEventInputStream hdfsInputStream;

    @Autowired
    private KafkaService kafkaService;

    @Value("${hdfs.dir}")
    private String dir;

    @SneakyThrows
    @Scheduled(cron = "* * * * * *")
    public void listen() {
        EventBatch eventBatch = hdfsInputStream.take();
        for (Event event : eventBatch.getEvents()) {
            switch (event.getEventType()) {
                case CREATE:
                    Event.CreateEvent createEvent = (Event.CreateEvent) event;
                    if (createEvent.getPath().startsWith(dir)) {
                        KafkaMessage kafkaMessage = KafkaMessage.builder()
                                .event(event.getEventType().name())
                                .id(UUID.randomUUID())
                                .url(createEvent.getPath())
                                .date(new Date(createEvent.getCtime()))
                                .build();
                        kafkaService.send(kafkaMessage);
                    }
                    break;
                case APPEND:
                    Event.AppendEvent appendEvent = (Event.AppendEvent) event;
                    if (appendEvent.getPath().startsWith(dir)) {
                        KafkaMessage kafkaMessage = KafkaMessage.builder()
                                .event(event.getEventType().name())
                                .id(UUID.randomUUID())
                                .url(appendEvent.getPath())
                                .date(new Date())
                                .build();
                        kafkaService.send(kafkaMessage);
                    }
                    break;
                case UNLINK:
                    break;
                case CLOSE:
                    break;
                case RENAME:
                    break;

                default:
                    break;
            }
        }
    }
}
