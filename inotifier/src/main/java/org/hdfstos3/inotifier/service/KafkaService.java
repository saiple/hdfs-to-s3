package org.hdfstos3.inotifier.service;

import org.hdfstos3.models.kafka.KafkaMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaService {

    @Value("${kafka.topic}")
    private String kafkaTopic;

    @Value("${tracing.enabled}")
    private Boolean tracing;

    @Autowired
    private KafkaTemplate<String, KafkaMessage> kafkaTemplate;

    private Logger logger = LoggerFactory.getLogger(KafkaService.class);

    public void send(KafkaMessage kafkaMessage) {
        if (tracing)
            logger.info("Prepared to send :" + kafkaMessage);
        kafkaTemplate.send(kafkaTopic, kafkaMessage);
    }
}
