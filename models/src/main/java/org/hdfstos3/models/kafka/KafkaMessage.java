package org.hdfstos3.models.kafka;

import lombok.*;
import org.apache.hadoop.hdfs.inotify.Event;

import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class KafkaMessage {
    private UUID id;
    private String event;
    private String url;
    private Date date;
}
