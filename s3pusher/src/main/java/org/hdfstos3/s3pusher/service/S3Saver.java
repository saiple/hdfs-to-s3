package org.hdfstos3.s3pusher.service;

import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.UploadObjectArgs;
import io.minio.errors.*;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.hdfstos3.models.kafka.KafkaMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Service
public class S3Saver {

    @Autowired
    private MinioClient minioClient;

    @Autowired
    public FileSystem hdfs;

    @Value("${tempSavingFolder}")
    private String tempSavingFolder;

    @Value("${minio.bucket}")
    private String bucketName;

    @Value("${hdfs.dir}")
    private String hdfsDir;

    @Value("${tracing.enabled}")
    private Boolean tracing;

    private Logger logger = LoggerFactory.getLogger(S3Saver.class);

    @KafkaListener(topics = "${kafka.topic}", containerFactory = "kafkaMessageKafkaListenerContainerFactory", groupId = "${kafka.groupId}")
    public void save(KafkaMessage kafkaMessage) throws IOException, ServerException, InsufficientDataException, InternalException, InvalidResponseException, InvalidKeyException, NoSuchAlgorithmException, XmlParserException, ErrorResponseException {
        String filename = kafkaMessage.getUrl().replace(hdfsDir, "").replace("/", "");
        String localUrl = tempSavingFolder + filename;

        if (tracing) {
            logger.info("Received: " + kafkaMessage);
            logger.info("Filename: {}, Full Path: {}", filename, localUrl);
        }
        hdfs.copyToLocalFile(new Path(kafkaMessage.getUrl()), new Path(localUrl));

        minioClient.uploadObject(
                UploadObjectArgs.builder()
                        .bucket(bucketName)
                        .object(filename)
                        .filename(localUrl)
                        .build());
        new File(localUrl).delete();
    }
}
