package org.hdfstos3.s3pusher.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("org.hdfstos3.s3pusher")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}
