package org.hdfstos3.s3pusher.configuration;

import io.minio.MinioClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;

@Configuration
@ConfigurationProperties(prefix = "minio")
@Data
public class MinioConfiguration {

    private String host;
    private int port;
    private boolean tls;
    private String accessKey;
    private String secretKey;

    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder()
                .endpoint(host, port, tls)
                .credentials(accessKey, secretKey)
                .build();
    }

}
