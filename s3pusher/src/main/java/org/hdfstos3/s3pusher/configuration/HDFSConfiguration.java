package org.hdfstos3.s3pusher.configuration;

import lombok.Data;
import lombok.SneakyThrows;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.hdfs.DFSClient;
import org.apache.hadoop.hdfs.DFSInotifyEventInputStream;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "hdfs")
@Data
public class HDFSConfiguration {
    private String url;
    private String user;
    private String dir;
    //private String hadoopHome;

    @SneakyThrows
    @Bean
    public FileSystem hdfsClient(){
        System.setProperty("HADOOP_USER_NAME", user);
    //    System.setProperty("hadoop.home.dir", hadoopHome);
        org.apache.hadoop.conf.Configuration configuration = new org.apache.hadoop.conf.Configuration();
        configuration.set("fs.default.name", url);
        return FileSystem.get(configuration);
    }

//    @SneakyThrows
//    @Bean
//    public DFSInotifyEventInputStream hdfsInputStream(DFSClient dfsClient){
//        return dfsClient.getInotifyEventStream(0);
//    }

}
