package org.hdfstos3.pushtohdfs;

import io.minio.MinioClient;
import io.minio.UploadObjectArgs;
import io.minio.errors.*;
import lombok.SneakyThrows;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class Main {
    public static void main(String[] args) {
        uploadToHDFS();
//        uploadToS3();
//        downloadFromHDFS();

    }

    @SneakyThrows
    public static void uploadToHDFS() {
        System.setProperty("HADOOP_USER_NAME", "hadoopuser");
        String DIR_NAME = "/test";

        Configuration configuration = new Configuration();
        configuration.set("fs.defaultFS", "hdfs://185.139.70.96:9000");
      //  configuration.set("hadoop.tmp.dir", "/var/app/hadoop/data");
        configuration.set("fs.hdfs.impl", DistributedFileSystem.class.getName());
        configuration.set("fs.file.impl", LocalFileSystem.class.getName());
        FileSystem filesystem = FileSystem.get(configuration);

        FileSystem fs = FileSystem.get(configuration);
        fs.mkdirs(new Path(DIR_NAME));
        fs.copyFromLocalFile(new Path("C:\\Users\\razil\\Desktop\\photo4.jpg"),
                new Path(DIR_NAME + "/photo12.jpg"));
        filesystem.close();
    }

    @SneakyThrows
    public static void uploadToS3() {
        MinioClient minioClient = MinioClient.builder()
                .endpoint("localhost", 9000, false)
                .credentials("AKIAIOSFODNN7EXAMPLE", "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY")
                .build();

        minioClient.uploadObject(
                UploadObjectArgs.builder()
                        .bucket("new")
                        .object("photo.jpg")
                        .filename("C:\\Users\\razil\\Desktop\\papka\\photo.jpg")
                        //  .contentType("image/jpeg")
                        .build());
    }

    @SneakyThrows
    public static void downloadFromHDFS(){
        String DIR_NAME = "/test";
        System.setProperty("HADOOP_USER_NAME", "hadoopuser");
        System.setProperty("hadoop.home.dir", "C:\\Users\\razil\\Desktop\\hadoop-3.2.1\\hadoop-3.2.1\\");
        //System.setProperty("HADOOP_HOME", "/");
//        String DIR_NAME = "/test";
//
        Configuration configuration = new Configuration();
        configuration.set("fs.defaultFS", "hdfs://185.139.70.96:9000");
        configuration.set("hadoop.tmp.dir", "/var/app/hadoop/data");
        configuration.set("fs.hdfs.impl", DistributedFileSystem.class.getName());
        configuration.set("fs.file.impl", LocalFileSystem.class.getName());
        //configuration.set("hadoop.home.dir", "C:\\Users\\razil\\Desktop\\hadoop-3.2.1\\hadoop-3.2.1\\");
        FileSystem filesystem = FileSystem.get(configuration);

        FileSystem fs = FileSystem.get(configuration);
        fs.copyFromLocalFile(new Path("C:\\Users\\razil\\Desktop\\papka\\photo.jpg"),
                new Path(DIR_NAME + "/photo4.jpg"));
        filesystem.close();
    }

}
