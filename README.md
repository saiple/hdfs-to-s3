# HDFS to S3

## Description

### Modules:
* inotifier
    * Needs to catches updates of HDFS filesystem and write to Kafka, if something changes. It writes only the link of the file, not the whole file.
* models
     * Contains common modules, that used in project.
* pushtohdfs
    * Simple module to test functionality of HDFS and S3(MINIO). There you can upload or download files.
* s3pusher
    * Catches messages from inotifier, downloads file from HDFS to temp storage and uploads it to S3. 

### How to set up

#### PREREQUIREMENTS

* It needs a fully functional Hadoop Cluster.  I tried to up hadoop in Docker, but I dont get any success. After that I rent 2 EC2 nodes, and set up 2-node Hadoop Cluster.      
     The tutorial that I used: [HERE](https://medium.com/@jootorres_11979/how-to-set-up-a-hadoop-3-2-1-multi-node-cluster-on-ubuntu-18-04-2-nodes-567ca44a3b12)
   
#### Setting up

1. Firstly, make sure that all properties (HDFS url, username, paths; local and relative paths; s3(minio) paths) was updated
2. Up zookeeper, kafka, and s3 (minio) in docker-compose file.
3. Create folder in HDFS and bucket in S3
4. Up inotifier and check availability to HDFS and Kafka
    * inotifier catches only creating of new files, it can be implemented on other events
    * inotifier catches updates each minute (it turned out so :) )
5. Up s3pusher and check availability to Kafka

### How to test
 * Send some file to HDFS via _pushtohdfs_ module. 
 * Check that inotifier catched the filesystem changes and send proper information to Kafka. 
 * Also check that s3pusher triggered and pushed file to S3.
 
### FAQ

* How to check what stored in HDFS and S3:  
    * HDFS: [http://{your-hadoop-cluster}:9870/](http://{your-hadoop-cluster}:9870/)
    * S3: [http://{your-s3-cluster}:9000/](http://{your-s3-cluster}:9870/) (Default: [http://localhost:9000/](http://localhost:9000/)  )


